

@tag
Feature: Login to OrangeHRM website
  Scenario: Login with valid credentials
    Given User launches the website
    And User Enters Username
    And User Enters Password
    When User clicks submit button
    Then User logged into application successfully
