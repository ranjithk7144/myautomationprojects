package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	private WebDriver driver;
	
	@FindBy(xpath = "//input[@id='txtUsername']")
	private static WebElement username;
	
	@FindBy(xpath = "//input[@id='txtPassword']")
	private static WebElement password;
	
	@FindBy(xpath = "//input[@id='btnLogin']")
	private static WebElement submit;
	
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void enterUsername()
	{
		username.sendKeys("Admin");
	}
	
	public void enterPassword()
	{
		password.sendKeys("admin123");
	}
	
	public void loginApplication()
	{
		submit.click();
	}
}
