package utilityFile;

import org.openqa.selenium.WebDriver;

public class TestEnvironment {
	private WebDriver driver;
	
	public void setup_environment(String environment)
	{
		if(environment.contains("orangehrmlive"))
		{
			DriverFactory.getDriver().get("https://opensource-demo.orangehrmlive.com/");
		}
		else if(environment.contains("google"))
		{
			DriverFactory.getDriver().get("https://www.google.com/");
		}
		else
		{
			System.out.println("Invalid browser");
		}
		
	}

}
