package utilityFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
	private Properties prop;
	
	public Properties init_prop() throws IOException 
	{
		try
		{
			FileInputStream ip= new FileInputStream("./Resources/config/config.properties");
			prop= new Properties();
			prop.load(ip);
			
		}
		catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return prop;
	}
}
