package applicationHooks;

import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import utilityFile.ConfigReader;
import utilityFile.DriverFactory;
import utilityFile.TestEnvironment;

public class AppHooks {
	private DriverFactory driverFactory;
	private TestEnvironment testEnvironment;
	private ConfigReader configReader;
	private WebDriver driver;
	Properties prop;

	@Before(order = 0)
	public void load_property() throws IOException
	{
		configReader= new ConfigReader();
		prop= configReader.init_prop();
	}
	
	@Before(order = 1)
	public void launch_browser() throws IOException
	{
		String browser_name=prop.getProperty("browser");
		driverFactory=new DriverFactory();
		driver=driverFactory.init_driver(browser_name);
	}
	@Before(order = 2)
	public void testEnvironment() throws IOException
	{
		String environment=prop.getProperty("URL");
		testEnvironment= new TestEnvironment();
		testEnvironment.setup_environment(environment);
	}
	 
}
