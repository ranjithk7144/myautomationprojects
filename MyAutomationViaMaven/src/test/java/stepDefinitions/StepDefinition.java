package stepDefinitions;

import org.openqa.selenium.WebDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.LoginPage;
import utilityFile.DriverFactory;

public class StepDefinition {
	private WebDriver driver;
	LoginPage login;
	
	@Given("User launches the website")
	public void user_launches_the_website() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Application Launched");
	}

	@And("User Enters Username")
	public void user_enters_username() {
	    // Write code here that turns the phrase above into concrete actions
		login= new LoginPage(DriverFactory.getDriver());
		login.enterUsername();
		
	}

	@And("User Enters Password")
	public void user_enters_password() {
	    // Write code here that turns the phrase above into concrete actions
		login.enterPassword();
	}

	@When("User clicks submit button")
	public void user_clicks_submit_button() {
	    // Write code here that turns the phrase above into concrete actions
		login.loginApplication();
	}

	@Then("User logged into application successfully")
	public void user_logged_into_application_successfully() {
	    // Write code here that turns the phrase above into concrete actions
		 System.out.println("Application is launched");
	}

}
